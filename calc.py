def suma(num1, num2):
    return num1 + num2


def resta(num1, num2):
    return num1 - num2


def calculadora():
    print("Suma de 1 y 2: " + str(suma(1, 2)))
    print("Suma de 3 y 4: " + str(suma(3, 4)))
    print("Resta 5 a 6: " + str(resta(6, 5)))
    print("Resta 7 a 8: " + str(resta(8, 7)))
